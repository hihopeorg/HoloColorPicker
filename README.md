# HoloColorPicker

**本项目是基于开源项目HoloColorPicker进行ohos的移植和开发的，可以通过项目标签以及github地址（https://github.com/LarsWerkman/HoloColorPicker ）追踪到原项目版本**

#### 项目介绍

- 项目名称：HoloColorPicker
- 所属系列：ohos的第三方组件适配移植
- 功能： 全息主题选色器，可设置颜色饱和度，亮度，不透明度
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/LarsWerkman/HoloColorPicker
- 原项目基线版本：sha1:8ba5971f8d25f80c22d9711713a51c7a7c4ac4aa
- 编程语言：Java
- 外部库依赖：无

#### 效果
<img src="screenshot/operation.gif"/>

#### 安装教程

方法一：

1. 编译har包HoloColorPicker.har。
2. 启动 DevEco Studio，将har包导入工程目录“app->libs”下。

3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。
```groovy
repositories {
    flatDir { dirs 'libs' }
}
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    implementation(name: 'HoloColorPicker', ext: 'har')

}
```

方法二：

1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址
```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/' 
    }
}
```
2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:
```
dependencies {
    implementation 'com.larswerkman.ohos:holocolorpicker:1.0.0'
}
```

#### 使用说明

```xml
        <StackLayout
               ohos:height="match_parent"
               ohos:width="match_parent">
               <com.larswerkman.holocolorpicker.ColorPicker
                   ohos:id="$+id:picker"
                   ohos:top_margin="30vp"
                   ohos:width="match_content"
                   ohos:height="match_content"
                   ohos:layout_alignment="center"/>
       
                   <com.larswerkman.holocolorpicker.SVBar
                       ohos:id="$+id:svbar"
                       ohos:width="322vp"
                       ohos:height="112vp"
                       ohos:layout_alignment="bottom"
                       ohos:bottom_margin="190vp"/>
       
                   <com.larswerkman.holocolorpicker.OpacityBar
                       ohos:id="$+id:opacitybar"
                       ohos:width="322vp"
                       ohos:height="112vp"
                       ohos:layout_alignment="bottom"
                       ohos:bottom_margin="130vp"/>
               <com.larswerkman.holocolorpicker.SaturationBar
                   ohos:id="$+id:saturationBar"
                   ohos:width="322vp"
                   ohos:height="112vp"
                   ohos:layout_alignment="bottom"
                   ohos:bottom_margin="90vp"/>
       
               <com.larswerkman.holocolorpicker.ValueBar
                   ohos:id="$+id:valueBar"
                   ohos:width="322vp"
                   ohos:height="112vp"
                   ohos:layout_alignment="bottom"
                   ohos:bottom_margin="40vp"/>
        </StackLayout>
```
#### 版本迭代


- 1.0.1


#### 版权和许可信息
```
Copyright 2012 Lars Werkman

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
```

