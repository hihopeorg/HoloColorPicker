/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.larswerkman.holocolorpicker;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.render.Path;
import ohos.app.Context;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * UI 自动化
 */
public class ExampleOhosTest {
    private Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
    private IAbilityDelegator iAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
    private MainAbility ability;


    @Before
    public void obtainAbility() {
        ability = EventHelper.startAbility(MainAbility.class);
        EventHelper.waitForActive(ability, 5);
    }

    @After
    public void tearDown() throws Exception {
        EventHelper.clearAbilities();
    }

    @Test
    public void testAbility() {
        assertNotNull("testAbility failed, can not start ability", ability);
    }

    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.larswerkman.holocolorpicker", actualBundleName);
    }


    @Test
    public void testColorPicker() {
        ColorPicker picker = (ColorPicker) ability.findComponentById(ResourceTable.Id_picker);
        SVBar svBar = (SVBar) ability.findComponentById(ResourceTable.Id_svbar);
        Path path = new Path();
        path.addCircle(90, 90, 372, Path.Direction.CLOCK_WISE);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        EventHelper.inputSwipe(ability, picker, path, 3000);
        assertEquals(svBar.getColor(), picker.getColor());
    }

    @Test
    public void testSVBar() {
        ColorPicker picker = (ColorPicker) ability.findComponentById(ResourceTable.Id_picker);
        SVBar svBar = (SVBar) ability.findComponentById(ResourceTable.Id_svbar);
        EventHelper.inputSwipe(ability, svBar, 30, 30, 900, 30, 3000);
        assertEquals(svBar.getColor(), picker.getColor());
    }

    @Test
    public void testOpacityBar() {
        ColorPicker picker = (ColorPicker) ability.findComponentById(ResourceTable.Id_picker);
        OpacityBar opacityBar = (OpacityBar) ability.findComponentById(ResourceTable.Id_opacitybar);
        EventHelper.inputSwipe(ability, opacityBar, 900, 30, 30, 30, 3000);
        assertEquals(opacityBar.getColor(), picker.getColor());
    }

    @Test
    public void testSaturationBar() {
        ColorPicker picker = (ColorPicker) ability.findComponentById(ResourceTable.Id_picker);
        SaturationBar saturationBar = (SaturationBar) ability.findComponentById(ResourceTable.Id_saturationBar);
        EventHelper.inputSwipe(ability, saturationBar, 900, 30, 30, 30, 3000);
        assertEquals(saturationBar.getColor(), picker.getColor());
    }

    @Test
    public void testValueBar() {
        ColorPicker picker = (ColorPicker) ability.findComponentById(ResourceTable.Id_picker);
        ValueBar valueBar = (ValueBar) ability.findComponentById(ResourceTable.Id_valueBar);
        EventHelper.inputSwipe(ability, valueBar, 30, 30, 900, 30, 3000);
        assertEquals(valueBar.getColor(), picker.getColor());
    }
}