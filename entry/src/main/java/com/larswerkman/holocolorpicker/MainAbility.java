package com.larswerkman.holocolorpicker;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;

/**
 * MainAbility
 */
public class MainAbility extends Ability implements ColorPicker.OnColorChangedListener {
    private ColorPicker picker;
    private SVBar svBar;
    private OpacityBar opacityBar;
    private Button button;
    private SaturationBar saturationBar;
    private ValueBar valueBar;
    private Text text;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_slice_sample);

        picker = (ColorPicker) findComponentById(ResourceTable.Id_picker);
        svBar = (SVBar) findComponentById(ResourceTable.Id_svbar);
        opacityBar = (OpacityBar) findComponentById(ResourceTable.Id_opacitybar);
        button = (Button) findComponentById(ResourceTable.Id_button1);
        text = (Text) findComponentById(ResourceTable.Id_textView1);
        saturationBar = (SaturationBar) findComponentById(ResourceTable.Id_saturationBar);
        valueBar = (ValueBar) findComponentById(ResourceTable.Id_valueBar);

        picker.addSVBar(svBar);
        picker.addOpacityBar(opacityBar);
        picker.addSaturationBar(saturationBar);
        picker.addValueBar(valueBar);
        picker.setOnColorChangedListener(this);

        button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                text.setTextColor(new Color(picker.getColor()));
                picker.setOldCenterColor(picker.getColor());
            }
        });
    }
    @Override
    public void onColorChanged(int color) {
        text.setTextColor(new Color(color));
    }
}
