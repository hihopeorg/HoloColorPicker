/*
 * Copyright 2012 Lars Werkman
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.larswerkman.holocolorpicker;

import ohos.agp.colors.HsvColor;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

/**
 * Displays a holo-themed color picker.
 *
 * <p>
 * Use {@link #getColor()} to retrieve the selected color. <br>
 * Use {@link #addSVBar(SVBar)} to add a Saturation/Value Bar. <br>
 * Use {@link #addOpacityBar(OpacityBar)} to add a Opacity Bar.
 * </p>
 */
public class ColorPicker extends Component implements Component.EstimateSizeListener, Component.DrawTask, Component.TouchEventListener {
    private static HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x0000101, "ColorPicker");
    /*
     * Constants used to save/restore the instance state.
     */
    private static final String STATE_PARENT = "parent";
    private static final String STATE_ANGLE = "angle";
    private static final String STATE_OLD_COLOR = "color";
    private static final String STATE_SHOW_OLD_COLOR = "showColor";

    /**
     * Colors to construct the color wheel using
     */
    private static final Color[] COLORS = new Color[]{new Color(0xFFFF0000), new Color(0xFFFF00FF),
            new Color(0xFF0000FF), new Color(0xFF00FFFF), new Color(0xFF00FF00), new Color(0xFFFFFF00), new Color(0xFFFF0000)};

    /**
     * {@code Paint} instance used to draw the color wheel.
     */
    private Paint mColorWheelPaint;

    /**
     * {@code Paint} instance used to draw the pointer's "halo".
     */
    private Paint mPointerHaloPaint;

    /**
     * {@code Paint} instance used to draw the pointer (the selected color).
     */
    private Paint mPointerColor;

    /**
     * The width of the color wheel thickness.
     */
    private int mColorWheelThickness;

    /**
     * The radius of the color wheel.
     */
    private int mColorWheelRadius;
    private int mPreferredColorWheelRadius;

    /**
     * The radius of the center circle inside the color wheel.
     */
    private int mColorCenterRadius;
    private int mPreferredColorCenterRadius;

    /**
     * The radius of the halo of the center circle inside the color wheel.
     */
    private int mColorCenterHaloRadius;
    private int mPreferredColorCenterHaloRadius;

    /**
     * The radius of the pointer.
     */
    private int mColorPointerRadius;

    /**
     * The radius of the halo of the pointer.
     */
    private int mColorPointerHaloRadius;

    /**
     * The rectangle enclosing the color wheel.
     */
    private RectFloat mColorWheelRectangle = new RectFloat();

    /**
     * The rectangle enclosing the center inside the color wheel.
     */
    private RectFloat mCenterRectangle = new RectFloat();

    /**
     * {@code true} if the user clicked on the pointer to start the move mode. <br>
     * {@code false} once the user stops touching the screen.
     *
     * @see #onTouchEvent
     */
    private boolean mUserIsMovingPointer = false;

    /**
     * The ARGB value of the currently selected color.
     */
    private int mColor;

    /**
     * The ARGB value of the center with the old selected color.
     */
    private int mCenterOldColor;

    /**
     * Whether to show the old color in the center or not.
     */
    private boolean mShowCenterOldColor;

    /**
     * The ARGB value of the center with the new selected color.
     */
    private int mCenterNewColor;

    /**
     * Number of pixels the origin of this view is moved in X- and Y-direction.
     *
     * <p>
     * We use the center of this (quadratic) View as origin of our internal
     * coordinate system. Uses the upper left corner as origin for the
     * View-specific coordinate system. So this is the value we use to translate
     * from one coordinate system to the other.
     * </p>
     *
     * <p>
     * Note: (Re)calculated in
     * </p>
     *
     * @see #onDraw
     */
    private float mTranslationOffset;

    /**
     * Distance between pointer and user touch in X-direction.
     */
    private float mSlopX;

    /**
     * Distance between pointer and user touch in Y-direction.
     */
    private float mSlopY;

    /**
     * The pointer's position expressed as angle (in rad).
     */
    private float mAngle;

    /**
     * {@code Paint} instance used to draw the center with the old selected
     * color.
     */
    private Paint mCenterOldPaint;

    /**
     * {@code Paint} instance used to draw the center with the new selected
     * color.
     */
    private Paint mCenterNewPaint;

    /**
     * {@code Paint} instance used to draw the halo of the center selected
     * colors.
     */
    private Paint mCenterHaloPaint;

    /**
     * An array of floats that can be build into a {@code Color} <br>
     * Where we can extract the Saturation and Value from.
     */
    private HsvColor mHSV;

    /**
     * {@code SVBar} instance used to control the Saturation/Value bar.
     */
    private SVBar mSVbar = null;

    /**
     * {@code OpacityBar} instance used to control the Opacity bar.
     */
    private OpacityBar mOpacityBar = null;

    /**
     * {@code SaturationBar} instance used to control the Saturation bar.
     */
    private SaturationBar mSaturationBar = null;

    /**
     * {@code TouchAnywhereOnColorWheelEnabled} instance used to control <br>
     * if the color wheel accepts input anywhere on the wheel or just <br>
     * on the halo.
     */
    private boolean mTouchAnywhereOnColorWheelEnabled = true;

    /**
     * {@code ValueBar} instance used to control the Value bar.
     */
    private ValueBar mValueBar = null;

    /**
     * {@code onColorChangedListener} instance of the onColorChangedListener
     */
    private OnColorChangedListener onColorChangedListener;

    /**
     * {@code onColorSelectedListener} instance of the onColorSelectedListener
     */
    private OnColorSelectedListener onColorSelectedListener;

    public ColorPicker(Context context) {
        super(context);
        initPaint();
    }

    public ColorPicker(Context context, AttrSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public ColorPicker(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(attrSet);
    }

    public ColorPicker(Context context, AttrSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    /**
     * An interface that is called whenever the color is changed. Currently it
     * is always called when the color is changes.
     *
     * @author lars
     */
    public interface OnColorChangedListener {
        public void onColorChanged(int color);
    }

    /**
     * An interface that is called whenever a new color has been selected.
     * Currently it is always called when the color wheel has been released.
     */
    public interface OnColorSelectedListener {
        public void onColorSelected(int color);
    }

    /**
     * Set a onColorChangedListener
     *
     * @param listener {@code OnColorChangedListener}
     */
    public void setOnColorChangedListener(OnColorChangedListener listener) {
        this.onColorChangedListener = listener;
    }

    /**
     * Gets the onColorChangedListener
     *
     * @return {@code OnColorChangedListener}
     */
    public OnColorChangedListener getOnColorChangedListener() {
        return this.onColorChangedListener;
    }

    /**
     * Set a onColorSelectedListener
     *
     * @param listener {@code OnColorSelectedListener}
     */
    public void setOnColorSelectedListener(OnColorSelectedListener listener) {
        this.onColorSelectedListener = listener;
    }

    /**
     * Gets the onColorSelectedListener
     *
     * @return {@code OnColorSelectedListener}
     */
    public OnColorSelectedListener getOnColorSelectedListener() {
        return this.onColorSelectedListener;
    }

    /**
     * Color of the latest entry of the onColorChangedListener.
     */
    private int oldChangedListenerColor;

    /**
     * Color of the latest entry of the onColorSelectedListener.
     */
    private int oldSelectedListenerColor;

    private float DEFAULT_THICKNESS = 24;
    private float DEFAULT_RADIUS = 124 * 3;
    private float DEFAULT_CENTER_RADIUS = 54 * 3;
    private float DEFAULT_CENTER_HALO_RADIUS = 60 * 3;
    private float DEFAULT_POINTER_RADIUS = 14 * 3;
    private float DEFAULT_POINTER_HALO_RADIUS = 18 * 3;

    private void init(AttrSet attrs) {
        mColorWheelThickness = (int) AttrSetUtil.getDimension(attrs, AttrConstant.COLOR_WHEEL_THICKNESS, DEFAULT_THICKNESS);
        mColorWheelRadius = (int) AttrSetUtil.getDimension(attrs, AttrConstant.COLOR_WHEEL_RADIUS, DEFAULT_RADIUS);
        mPreferredColorWheelRadius = mColorWheelRadius;
        mColorCenterRadius = (int) AttrSetUtil.getDimension(attrs, AttrConstant.COLOR_CENTER_RADIUS, DEFAULT_CENTER_RADIUS);
        mPreferredColorCenterRadius = mColorCenterRadius;
        mColorCenterHaloRadius = (int) AttrSetUtil.getDimension(attrs, AttrConstant.COLOR_CENTER_HALO_RADIUS, DEFAULT_CENTER_HALO_RADIUS);
        mPreferredColorCenterHaloRadius = mColorCenterHaloRadius;
        mColorPointerRadius = (int) AttrSetUtil.getDimension(attrs, AttrConstant.COLOR_POINTER_RADIUS, DEFAULT_POINTER_RADIUS);
        mColorPointerHaloRadius = (int) AttrSetUtil.getDimension(attrs, AttrConstant.COLOR_POINTER_HALO_RADIUS, DEFAULT_POINTER_HALO_RADIUS);

        initPaint();

    }

    private void initPaint() {
        mAngle = (float) (-Math.PI / 2);
        Shader s = new SweepShader(0, 0, COLORS, null);

        mColorWheelPaint = new Paint();
        mColorWheelPaint.setAntiAlias(true);
        mColorWheelPaint.setShader(s, Paint.ShaderType.SWEEP_SHADER);
        mColorWheelPaint.setStyle(Paint.Style.STROKE_STYLE);
        mColorWheelPaint.setStrokeWidth(mColorWheelThickness);

        mPointerHaloPaint = new Paint();
        mPointerHaloPaint.setAntiAlias(true);
        mPointerHaloPaint.setColor(Color.BLACK);
        mPointerHaloPaint.setAlpha(0.5f);

        mPointerColor = new Paint();
        mPointerColor.setAntiAlias(true);
        mPointerColor.setColor(new Color(calculateColor(mAngle)));

        mCenterNewPaint = new Paint();
        mCenterNewPaint.setAntiAlias(true);
        mCenterNewPaint.setColor(new Color(calculateColor(mAngle)));
        mCenterNewPaint.setStyle(Paint.Style.FILL_STYLE);

        mCenterOldPaint = new Paint();
        mCenterOldPaint.setAntiAlias(true);
        mCenterOldPaint.setColor(new Color(calculateColor(mAngle)));
        mCenterOldPaint.setStyle(Paint.Style.FILL_STYLE);

        mCenterHaloPaint = new Paint();
        mCenterHaloPaint.setAntiAlias(true);
        mCenterHaloPaint.setColor(Color.BLACK);
        mCenterHaloPaint.setAlpha(0x00);

        mCenterNewColor = calculateColor(mAngle);
        mCenterOldColor = calculateColor(mAngle);
        mShowCenterOldColor = true;
        HiLog.info(label, "init()");
        setEstimateSizeListener(this);
        setTouchEventListener(this);
        addDrawTask(this);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        HiLog.info(label, "onDraw");
        HiLog.info(label, "onDraw mColorCenterHaloRadius:%{public}d", mColorCenterHaloRadius);
        // All of our positions are using our internal coordinate system.
        // Instead of translating
        // them we let Canvas do the work for us.
        canvas.translate(mTranslationOffset, mTranslationOffset);

        // Draw the color wheel.
        float width = mColorWheelRectangle.right;
        float height = mColorWheelRectangle.bottom;
        HiLog.info(label, "onDraw mColorWheelRectangle:%{public}f,%{public}f", width, height);
        canvas.drawOval(mColorWheelRectangle, mColorWheelPaint);

        float[] pointerPosition = calculatePointerPosition(mAngle);

        // Draw the pointer's "halo"
        canvas.drawCircle(pointerPosition[0], pointerPosition[1],
                mColorPointerHaloRadius, mPointerHaloPaint);

        // Draw the pointer (the currently selected color) slightly smaller on
        // top.
        canvas.drawCircle(pointerPosition[0], pointerPosition[1],
                mColorPointerRadius, mPointerColor);

        // Draw the halo of the center colors.
        canvas.drawCircle(0, 0, mColorCenterHaloRadius, mCenterHaloPaint);

        if (mShowCenterOldColor) {
            // Draw the old selected color in the center.
            canvas.drawArc(mCenterRectangle, new Arc(90, 180, true), mCenterOldPaint);

            // Draw the new selected color in the center.
            canvas.drawArc(mCenterRectangle, new Arc(270, 180, true), mCenterNewPaint);
        } else {
            // Draw the new selected color in the center.
            canvas.drawArc(mCenterRectangle, new Arc(0, 360, true), mCenterNewPaint);
        }
    }

    @Override
    public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {
        HiLog.info(label, "onEstimateSize:%{public}d,%{public}d", widthMeasureSpec, heightMeasureSpec);
        final int intrinsicSize = 2 * (mPreferredColorWheelRadius + mColorPointerHaloRadius);

        int widthMode = EstimateSpec.getMode(widthMeasureSpec);
        int widthSize = EstimateSpec.getSize(widthMeasureSpec);
        int heightMode = EstimateSpec.getMode(heightMeasureSpec);
        int heightSize = EstimateSpec.getSize(heightMeasureSpec);
        HiLog.info(label, "EstimateSpec:%{public}d,%{public}d", widthSize, heightSize);
        int width;
        int height;

        if (widthMode == EstimateSpec.PRECISE) {
            width = widthSize;
        } else if (widthMode == EstimateSpec.NOT_EXCEED) {
            width = Math.min(intrinsicSize, widthSize);
        } else {
            width = intrinsicSize;
        }

        if (heightMode == EstimateSpec.PRECISE) {
            height = heightSize;
        } else if (heightMode == EstimateSpec.NOT_EXCEED) {
            height = Math.min(intrinsicSize, heightSize);
        } else {
            height = intrinsicSize;
        }

        int min = Math.min(width, height);
        setEstimatedSize(
                Component.EstimateSpec.getChildSizeWithMode(min, min, widthMode),
                Component.EstimateSpec.getChildSizeWithMode(min, min, heightMode)
        );
//        setEstimatedSize(min, min);
        HiLog.info(label, "setEstimatedSize:%{public}d,%{public}d", min, min);
        mTranslationOffset = min * 0.5f;

        // fill the rectangle instances.
        mColorWheelRadius = min / 2 - mColorWheelThickness - mColorPointerHaloRadius;
        mColorWheelRectangle.modify(-mColorWheelRadius, -mColorWheelRadius,
                mColorWheelRadius, mColorWheelRadius);

        mColorCenterRadius = (int) ((float) mPreferredColorCenterRadius * ((float) mColorWheelRadius / (float) mPreferredColorWheelRadius));
        mColorCenterHaloRadius = (int) ((float) mPreferredColorCenterHaloRadius * ((float) mColorWheelRadius / (float) mPreferredColorWheelRadius));
        mCenterRectangle.modify(-mColorCenterRadius, -mColorCenterRadius,
                mColorCenterRadius, mColorCenterRadius);
        return true;
    }

    @Override
    public void invalidate() {
        addDrawTask(this);
        super.invalidate();
    }

    private int ave(int s, int d, float p) {
        return s + Math.round(p * (d - s));
    }

    /**
     * Calculate the color using the supplied angle.
     *
     * @param angle The selected color's position expressed as angle (in rad).
     * @return The ARGB value of the color on the color wheel at the specified
     * angle.
     */
    private int calculateColor(float angle) {
        float unit = (float) (angle / (2 * Math.PI));
        if (unit < 0) {
            unit += 1;
        }

        if (unit <= 0) {
            mColor = COLORS[0].getValue();
            return COLORS[0].getValue();
        }
        if (unit >= 1) {
            mColor = COLORS[COLORS.length - 1].getValue();
            return COLORS[COLORS.length - 1].getValue();
        }

        float p = unit * (COLORS.length - 1);
        int i = (int) p;
        p -= i;

        int c0 = COLORS[i].getValue();
        int c1 = COLORS[i + 1].getValue();

        int a = ave(RgbColor.fromArgbInt(c0).getAlpha(), RgbColor.fromArgbInt(c1).getAlpha(), p);
        int r = ave(RgbColor.fromArgbInt(c0).getRed(), RgbColor.fromArgbInt(c1).getRed(), p);
        int g = ave(RgbColor.fromArgbInt(c0).getGreen(), RgbColor.fromArgbInt(c1).getGreen(), p);
        int b = ave(RgbColor.fromArgbInt(c0).getBlue(), RgbColor.fromArgbInt(c1).getBlue(), p);

        mColor = Color.argb(a, r, g, b);
        return Color.argb(a, r, g, b);
    }

    /**
     * Get the currently selected color.
     *
     * @return The ARGB value of the currently selected color.
     */
    public int getColor() {
        return mCenterNewColor;
    }

    /**
     * Set the color to be highlighted by the pointer. If the
     * instances {@code SVBar} and the {@code OpacityBar} aren't null the color
     * will also be set to them
     *
     * @param color The RGB value of the color to highlight. If this is not a
     *              color displayed on the color wheel a very simple algorithm is
     *              used to map it to the color wheel. The resulting color often
     *              won't look close to the original color. This is especially
     *              true for shades of grey. You have been warned!
     */
    public void setColor(int color) {
        mAngle = colorToAngle(color);
        mPointerColor.setColor(new Color(calculateColor(mAngle)));

        // check of the instance isn't null
        if (mOpacityBar != null) {
            // set the value of the opacity
            mOpacityBar.setColor(mColor);
            mOpacityBar.setOpacity(Color.alpha(color));
        }

        // check if the instance isn't null
        if (mSVbar != null) {
            // the array mHSV will be filled with the HSV values of the color.
            mHSV = HsvColor.toHSV(color);
            mSVbar.setColor(mColor);

            // because of the design of the Saturation/Value bar,
            // we can only use Saturation or Value every time.
            // Here will be checked which we shall use.
            if (mHSV.getSaturation() < mHSV.getValue()) {
                mSVbar.setSaturation(mHSV.getSaturation());
            } else if (mHSV.getSaturation() > mHSV.getValue()) {
                mSVbar.setValue(mHSV.getValue());
            }
        }

        if (mSaturationBar != null) {
            mHSV = HsvColor.toHSV(color);
            mSaturationBar.setColor(mColor);
            mSaturationBar.setSaturation(mHSV.getSaturation());
        }

        if (mValueBar != null && mSaturationBar == null) {
            mHSV = HsvColor.toHSV(color);
            mValueBar.setColor(mColor);
            mValueBar.setValue(mHSV.getValue());
        } else if (mValueBar != null) {
            mHSV = HsvColor.toHSV(color);
            mValueBar.setValue(mHSV.getValue());
        }
        setNewCenterColor(color);
    }

    /**
     * Convert a color to an angle.
     *
     * @param color The RGB value of the color to "find" on the color wheel.
     * @return The angle (in rad) the "normalized" color is displayed on the
     * color wheel.
     */
    private float colorToAngle(int color) {
        HsvColor hsvColor = HsvColor.toHSV(color);
        return (float) Math.toRadians(-hsvColor.getHue());
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent event) {
        // Convert coordinates to our internal coordinate system
        MmiPoint point = event.getPointerPosition(0);

        // Convert coordinates to our internal coordinate system
        float x = point.getX() - mTranslationOffset;
        float y = point.getY() - mTranslationOffset;

        switch (event.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                // Check whether the user pressed on the pointer.
                float[] pointerPosition = calculatePointerPosition(mAngle);
                if (x >= (pointerPosition[0] - mColorPointerHaloRadius)
                        && x <= (pointerPosition[0] + mColorPointerHaloRadius)
                        && y >= (pointerPosition[1] - mColorPointerHaloRadius)
                        && y <= (pointerPosition[1] + mColorPointerHaloRadius)) {
                    mSlopX = x - pointerPosition[0];
                    mSlopY = y - pointerPosition[1];
                    mUserIsMovingPointer = true;
                    invalidate();
                }
                // Check whether the user pressed on the center.
                else if (x >= -mColorCenterRadius && x <= mColorCenterRadius
                        && y >= -mColorCenterRadius && y <= mColorCenterRadius
                        && mShowCenterOldColor) {
                    mCenterHaloPaint.setAlpha(0.5f);
                    setColor(getOldCenterColor());
                    invalidate();
                }
                // Check whether the user pressed anywhere on the wheel.
                else if (Math.sqrt(x * x + y * y) <= mColorWheelRadius + mColorPointerHaloRadius
                        && Math.sqrt(x * x + y * y) >= mColorWheelRadius - mColorPointerHaloRadius
                        && mTouchAnywhereOnColorWheelEnabled) {
                    mUserIsMovingPointer = true;
                    invalidate();
                }
                // If user did not press pointer or center, report event not handled
//				else{
//					getParent().requestDisallowInterceptTouchEvent(false);
//					return false;
//				}
                break;
            case TouchEvent.POINT_MOVE:
                if (mUserIsMovingPointer) {
                    mAngle = (float) Math.atan2(y - mSlopY, x - mSlopX);
                    mPointerColor.setColor(new Color(calculateColor(mAngle)));

                    setNewCenterColor(mCenterNewColor = calculateColor(mAngle));

                    if (mOpacityBar != null) {
                        mOpacityBar.setColor(mColor);
                    }

                    if (mValueBar != null) {
                        mValueBar.setColor(mColor);
                    }

                    if (mSaturationBar != null) {
                        mSaturationBar.setColor(mColor);
                    }

                    if (mSVbar != null) {
                        mSVbar.setColor(mColor);
                    }

                    invalidate();
                }
                // If user did not press pointer or center, report event not handled
//				else{
//					getParent().requestDisallowInterceptTouchEvent(false);
//					return false;
//				}
                break;
            case TouchEvent.PRIMARY_POINT_UP:
                mUserIsMovingPointer = false;
                mCenterHaloPaint.setAlpha(0x00);

                if (onColorSelectedListener != null && mCenterNewColor != oldSelectedListenerColor) {
                    onColorSelectedListener.onColorSelected(mCenterNewColor);
                    oldSelectedListenerColor = mCenterNewColor;
                }

                invalidate();
                break;
            case TouchEvent.CANCEL:
                if (onColorSelectedListener != null && mCenterNewColor != oldSelectedListenerColor) {
                    onColorSelectedListener.onColorSelected(mCenterNewColor);
                    oldSelectedListenerColor = mCenterNewColor;
                }
                break;
        }
        return true;
    }

    /**
     * Calculate the pointer's coordinates on the color wheel using the supplied
     * angle.
     *
     * @param angle The position of the pointer expressed as angle (in rad).
     * @return The coordinates of the pointer's center in our internal
     * coordinate system.
     */
    private float[] calculatePointerPosition(float angle) {
        float x = (float) (mColorWheelRadius * Math.cos(angle));
        float y = (float) (mColorWheelRadius * Math.sin(angle));

        return new float[]{x, y};
    }

    /**
     * Add a Saturation/Value bar to the color wheel.
     *
     * @param bar The instance of the Saturation/Value bar.
     */
    public void addSVBar(SVBar bar) {
        mSVbar = bar;
        // Give an instance of the color picker to the Saturation/Value bar.
        mSVbar.setColorPicker(this);
        mSVbar.setColor(mColor);
    }

    /**
     * Add a Opacity bar to the color wheel.
     *
     * @param bar The instance of the Opacity bar.
     */
    public void addOpacityBar(OpacityBar bar) {
        mOpacityBar = bar;
        // Give an instance of the color picker to the Opacity bar.
        mOpacityBar.setColorPicker(this);
        mOpacityBar.setColor(mColor);
    }

    public void addSaturationBar(SaturationBar bar) {
        mSaturationBar = bar;
        mSaturationBar.setColorPicker(this);
        mSaturationBar.setColor(mColor);
    }

    public void addValueBar(ValueBar bar) {
        mValueBar = bar;
        mValueBar.setColorPicker(this);
        mValueBar.setColor(mColor);
    }

    /**
     * Change the color of the center which indicates the new color.
     *
     * @param color int of the color.
     */
    public void setNewCenterColor(int color) {
        mCenterNewColor = color;
        mCenterNewPaint.setColor(new Color(color));
        if (mCenterOldColor == 0) {
            mCenterOldColor = color;
            mCenterOldPaint.setColor(new Color(color));
        }
        if (onColorChangedListener != null && color != oldChangedListenerColor) {
            onColorChangedListener.onColorChanged(color);
            oldChangedListenerColor = color;
        }
        invalidate();
    }

    /**
     * Change the color of the center which indicates the old color.
     *
     * @param color int of the color.
     */
    public void setOldCenterColor(int color) {
        mCenterOldColor = color;
        mCenterOldPaint.setColor(new Color(color));
        invalidate();
    }

    public int getOldCenterColor() {
        return mCenterOldColor;
    }

    /**
     * Set whether the old color is to be shown in the center or not
     *
     * @param show true if the old color is to be shown, false otherwise
     */
    public void setShowOldCenterColor(boolean show) {
        mShowCenterOldColor = show;
        invalidate();
    }

    public boolean getShowOldCenterColor() {
        return mShowCenterOldColor;
    }

    /**
     * Used to change the color of the {@code OpacityBar} used by the
     * {@code SVBar} if there is an change in color.
     *
     * @param color int of the color used to change the opacity bar color.
     */
    public void changeOpacityBarColor(int color) {
        if (mOpacityBar != null) {
            mOpacityBar.setColor(color);
        }
    }

    /**
     * Used to change the color of the {@code SaturationBar}.
     *
     * @param color int of the color used to change the opacity bar color.
     */
    public void changeSaturationBarColor(int color) {
        if (mSaturationBar != null) {
            mSaturationBar.setColor(color);
        }
    }

    /**
     * Used to change the color of the {@code ValueBar}.
     *
     * @param color int of the color used to change the opacity bar color.
     */
    public void changeValueBarColor(int color) {
        if (mValueBar != null) {
            mValueBar.setColor(color);
        }
    }

    /**
     * Checks if there is an {@code OpacityBar} connected.
     *
     * @return true or false.
     */
    public boolean hasOpacityBar() {
        return mOpacityBar != null;
    }

    /**
     * Checks if there is a {@code ValueBar} connected.
     *
     * @return true or false.
     */
    public boolean hasValueBar() {
        return mValueBar != null;
    }

    /**
     * Checks if there is a {@code SaturationBar} connected.
     *
     * @return true or false.
     */
    public boolean hasSaturationBar() {
        return mSaturationBar != null;
    }

    /**
     * Checks if there is a {@code SVBar} connected.
     *
     * @return true or false.
     */
    public boolean hasSVBar() {
        return mSVbar != null;
    }

    public void setTouchAnywhereOnColorWheelEnabled(boolean TouchAnywhereOnColorWheelEnabled) {
        mTouchAnywhereOnColorWheelEnabled = TouchAnywhereOnColorWheelEnabled;
    }

    public boolean getTouchAnywhereOnColorWheel() {
        return mTouchAnywhereOnColorWheelEnabled;
    }
}
