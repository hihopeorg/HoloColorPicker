/*
 * Copyright 2012 Lars Werkman
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.larswerkman.holocolorpicker;


import ohos.agp.colors.ColorConverter;
import ohos.agp.colors.HsvColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.render.Canvas;
import ohos.agp.render.LinearShader;
import ohos.agp.render.Paint;
import ohos.agp.render.Shader;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

public class ValueBar extends ComponentContainer implements Component.EstimateSizeListener, ComponentContainer.ArrangeListener, Component.DrawTask, Component.TouchEventListener {
    private static HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x0000100, "ValueBar");

    /*
     * Constants used to save/restore the instance state.
     */
    private static final String STATE_PARENT = "parent";
    private static final String STATE_COLOR = "color";
    private static final String STATE_VALUE = "value";
    private static final String STATE_ORIENTATION = "orientation";

    /**
     * Constants used to identify orientation.
     */
    private static final boolean ORIENTATION_HORIZONTAL = true;
    private static final boolean ORIENTATION_VERTICAL = false;

    /**
     * Default orientation of the bar.
     */
    private static final boolean ORIENTATION_DEFAULT = ORIENTATION_HORIZONTAL;

    /**
     * The thickness of the bar.
     */
    private int mBarThickness;

    /**
     * The length of the bar.
     */
    private int mBarLength;
    private int mPreferredBarLength;

    /**
     * The radius of the pointer.
     */
    private int mBarPointerRadius;

    /**
     * The radius of the halo of the pointer.
     */
    private int mBarPointerHaloRadius;

    /**
     * The position of the pointer on the bar.
     */
    private int mBarPointerPosition;

    /**
     * {@code Paint} instance used to draw the bar.
     */
    private Paint mBarPaint;

    /**
     * {@code Paint} instance used to draw the pointer.
     */
    private Paint mBarPointerPaint;

    /**
     * {@code Paint} instance used to draw the halo of the pointer.
     */
    private Paint mBarPointerHaloPaint;

    /**
     * The rectangle enclosing the bar.
     */
    private RectFloat mBarRect = new RectFloat();

    /**
     * {@code Shader} instance used to fill the shader of the paint.
     */
    private Shader shader;

    /**
     * {@code true} if the user clicked on the pointer to start the move mode. <br>
     * {@code false} once the user stops touching the screen.
     *
     * @see #onTouchEvent
     */
    private boolean mIsMovingPointer;

    /**
     * The ARGB value of the currently selected color.
     */
    private int mColor;

    /**
     * An array of floats that can be build into a {@code Color} <br>
     * Where we can extract the color from.
     */
    private HsvColor mHSVColor;

    /**
     * Factor used to calculate the position to the Opacity on the bar.
     */
    private float mPosToSatFactor;

    /**
     * Factor used to calculate the Opacity to the postion on the bar.
     */
    private float mSatToPosFactor;

    /**
     * {@code ColorPicker} instance used to control the ColorPicker.
     */
    private ColorPicker mPicker = null;

    /**
     * Used to toggle orientation between vertical and horizontal.
     */
    private boolean mOrientation;

    /**
     * Interface and listener so that changes in ValueBar are sent
     * to the host activity/fragment
     */
    private OnValueChangedListener onValueChangedListener;

    /**
     * Value of the latest entry of the onValueChangedListener.
     */
    private int oldChangedListenerValue;

    private float DEFAULT_THICKNESS = 12;
    private int DEFAULT_LENGTH = 240;


    public interface OnValueChangedListener {
        public void onValueChanged(int value);
    }

    public void setOnValueChangedListener(OnValueChangedListener listener) {
        this.onValueChangedListener = listener;
    }

    public OnValueChangedListener getOnValueChangedListener() {
        return this.onValueChangedListener;
    }

    public ValueBar(Context context) {
        super(context);
        initPaint();
    }

    public ValueBar(Context context, AttrSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public ValueBar(Context context, AttrSet attrs, String defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    private void init(AttrSet attrs) {

        mBarThickness = (int) AttrSetUtil.getDimension(attrs, AttrConstant.BAR_THICKNESS, DEFAULT_THICKNESS);
        mBarLength = (int) AttrSetUtil.getDimension(attrs, AttrConstant.BAR_LENGTH, DEFAULT_LENGTH);
        mPreferredBarLength = mBarLength;
        mBarPointerRadius = (int) AttrSetUtil.getDimension(attrs, AttrConstant.BAR_POINTER_RADIUS, 18);
        mBarPointerHaloRadius = (int) AttrSetUtil.getDimension(attrs, AttrConstant.BAR_POINTER_HALO_RADIUS, 42);
        mOrientation = AttrSetUtil.getBoolean(attrs, AttrConstant.BAR_ORIENTATION_HORIZONTAL, ORIENTATION_HORIZONTAL);
        initPaint();
    }

    private void initPaint() {


        mBarPaint = new Paint();
        mBarPaint.setAntiAlias(true);
        mBarPaint.setShader(shader, Paint.ShaderType.LINEAR_SHADER);

        mBarPointerPosition = mBarPointerHaloRadius;

        mBarPointerHaloPaint = new Paint();
        mBarPointerHaloPaint.setAntiAlias(true);
        mBarPointerHaloPaint.setColor(Color.BLACK);
        mBarPointerHaloPaint.setAlpha(0.5f);

        mBarPointerPaint = new Paint();
        mBarPointerPaint.setAntiAlias(true);
        mBarPointerPaint.setColor(new Color(0xff81ff00));

        mPosToSatFactor = 1 / ((float) mBarLength);
        mSatToPosFactor = ((float) mBarLength) / 1;
        setEstimateSizeListener(this::onEstimateSize);
        setTouchEventListener(this::onTouchEvent);
        setArrangeListener(this::onArrange);
        addDrawTask(this::onDraw);
    }

    @Override
    public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {
        HiLog.info(label, "onEstimateSize:%{public}d,height:%{public}d", widthMeasureSpec, heightMeasureSpec);
        final int intrinsicSize = mPreferredBarLength
                + (mBarPointerHaloRadius * 2);

        // Variable orientation
        int measureSpec;
        if (mOrientation == ORIENTATION_HORIZONTAL) {
            measureSpec = widthMeasureSpec;
        } else {
            measureSpec = heightMeasureSpec;
        }
        int lengthMode = EstimateSpec.getMode(measureSpec);
        int lengthSize = EstimateSpec.getSize(measureSpec);

        int length;
        if (lengthMode == EstimateSpec.PRECISE) {
            length = lengthSize;
        } else if (lengthMode == EstimateSpec.NOT_EXCEED) {
            length = Math.min(intrinsicSize, lengthSize);
        } else {
            length = intrinsicSize;
        }

        int barPointerHaloRadiusx2 = mBarPointerHaloRadius * 2;
        mBarLength = length - barPointerHaloRadiusx2;
        if (mOrientation == ORIENTATION_VERTICAL) {
            setEstimatedSize(barPointerHaloRadiusx2,
                    (mBarLength + barPointerHaloRadiusx2));
        } else {
            setEstimatedSize((mBarLength + barPointerHaloRadiusx2),
                    barPointerHaloRadiusx2);
        }
        return false;
    }

    @Override
    public boolean onArrange(int i, int i1, int i2, int i3) {
        int w = i2;
        int h = i3;

        // Fill the rectangle instance based on orientation
        int x1, y1;
        if (mOrientation == ORIENTATION_HORIZONTAL) {
            x1 = (mBarLength + mBarPointerHaloRadius);
            y1 = mBarThickness;
            mBarLength = w - (mBarPointerHaloRadius * 2);
            mBarRect.modify(mBarPointerHaloRadius,
                    (mBarPointerHaloRadius - (mBarThickness / 2)),
                    (mBarLength + (mBarPointerHaloRadius)),
                    (mBarPointerHaloRadius + (mBarThickness / 2)));
        } else {
            x1 = mBarThickness;
            y1 = (mBarLength + mBarPointerHaloRadius);
            mBarLength = h - (mBarPointerHaloRadius * 2);
            mBarRect.modify((mBarPointerHaloRadius - (mBarThickness / 2)),
                    mBarPointerHaloRadius,
                    (mBarPointerHaloRadius + (mBarThickness / 2)),
                    (mBarLength + (mBarPointerHaloRadius)));
        }

        shader = new LinearShader(new Point[]{new Point(mBarPointerHaloRadius, 0),new Point(x1,y1)}, new float[]{0,0,x1, y1},
                new Color[]{new Color(0xff81ff00), Color.BLACK}, Shader.TileMode.CLAMP_TILEMODE);
        mBarPaint.setShader(shader, Paint.ShaderType.LINEAR_SHADER);
        mPosToSatFactor = 1 / ((float) mBarLength);
        mSatToPosFactor = ((float) mBarLength) / 1;
        mColor = 0xff81ff00;
        mHSVColor = HsvColor.toHSV(mColor);
        mBarPointerPosition = Math
                .round((mBarLength - (mSatToPosFactor * (mHSVColor.getSaturation()/100)))
                        + mBarPointerHaloRadius);
        return false;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        canvas.drawRect(mBarRect, mBarPaint);

        // Calculate the center of the pointer.
        int cX, cY;
        if (mOrientation == ORIENTATION_HORIZONTAL) {
            cX = mBarPointerPosition;
            cY = mBarPointerHaloRadius;
        } else {
            cX = mBarPointerHaloRadius;
            cY = mBarPointerPosition;
        }

        // Draw the pointer halo.
        canvas.drawCircle(cX, cY, mBarPointerHaloRadius, mBarPointerHaloPaint);
        // Draw the pointer.
        canvas.drawCircle(cX, cY, mBarPointerRadius, mBarPointerPaint);
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent event) {
        // Convert coordinates to our internal coordinate system
        MmiPoint point = event.getPointerPosition(0);
        float dimen;
        if (mOrientation == ORIENTATION_HORIZONTAL) {
            dimen = point.getX();
        } else {
            dimen = point.getY();
        }

        switch (event.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                mIsMovingPointer = true;
                // Check whether the user pressed on (or near) the pointer
                if (dimen >= (mBarPointerHaloRadius)
                        && dimen <= (mBarPointerHaloRadius + mBarLength)) {
                    mBarPointerPosition = Math.round(dimen);
                    calculateColor(Math.round(dimen));
                    mBarPointerPaint.setColor(new Color(mColor));
                    invalidate();
                }
                break;
            case TouchEvent.POINT_MOVE:
                if (mIsMovingPointer) {
                    // Move the the pointer on the bar.
                    if (dimen >= mBarPointerHaloRadius
                            && dimen <= (mBarPointerHaloRadius + mBarLength)) {
                        mBarPointerPosition = Math.round(dimen);
                        calculateColor(Math.round(dimen));
                        mBarPointerPaint.setColor(new Color(mColor));
                        if (mPicker != null) {
                            mPicker.setNewCenterColor(mColor);
                            mPicker.changeOpacityBarColor(mColor);
                        }
                        invalidate();
                    } else if (dimen < mBarPointerHaloRadius) {
                        mBarPointerPosition = mBarPointerHaloRadius;
                        mColor = HsvColor.toColor(mHSVColor.getAlpha(), mHSVColor.getHue(), mHSVColor.getSaturation(), mHSVColor.getValue());
                        mBarPointerPaint.setColor(new Color(mColor));
                        if (mPicker != null) {
                            mPicker.setNewCenterColor(mColor);
                            mPicker.changeOpacityBarColor(mColor);
                        }
                        invalidate();
                    } else if (dimen > (mBarPointerHaloRadius + mBarLength)) {
                        mBarPointerPosition = mBarPointerHaloRadius + mBarLength;
                        mColor = Color.BLACK.getValue();
                        mBarPointerPaint.setColor(new Color(mColor));
                        if (mPicker != null) {
                            mPicker.setNewCenterColor(mColor);
                            mPicker.changeOpacityBarColor(mColor);
                        }
                        invalidate();
                    }
                }
                if (onValueChangedListener != null && oldChangedListenerValue != mColor) {
                    onValueChangedListener.onValueChanged(mColor);
                    oldChangedListenerValue = mColor;
                }
                break;
            case TouchEvent.PRIMARY_POINT_UP:
                mIsMovingPointer = false;
                break;
        }
        return true;
    }


    /**
     * Set the bar color. <br>
     * <br>
     * Its discouraged to use this method.
     *
     * @param color
     */
    public void setColor(int color) {
        int x1, y1;
        if (mOrientation == ORIENTATION_HORIZONTAL) {
            x1 = (mBarLength + mBarPointerHaloRadius);
            y1 = mBarThickness;
        } else {
            x1 = mBarThickness;
            y1 = (mBarLength + mBarPointerHaloRadius);
        }

        mHSVColor = HsvColor.toHSV(color);
        shader = new LinearShader(new Point[]{new Point(mBarPointerHaloRadius, 0), new Point(x1, y1)}, new float[]{0,0,x1, y1},
                new Color[]{new Color(color), Color.BLACK}, Shader.TileMode.CLAMP_TILEMODE);
        mBarPaint.setShader(shader, Paint.ShaderType.LINEAR_SHADER);
        calculateColor(mBarPointerPosition);
        mBarPointerPaint.setColor(new Color(mColor));
        if (mPicker != null) {
            mPicker.setNewCenterColor(mColor);
            if (mPicker.hasOpacityBar())
                mPicker.changeOpacityBarColor(mColor);
        }
        invalidate();
    }

    /**
     * Set the pointer on the bar. With the opacity value.
     *
     * @param value float between 0 and 1
     */
    public void setValue(float value) {
        mBarPointerPosition = Math
                .round((mBarLength - (mSatToPosFactor * value))
                        + mBarPointerHaloRadius);
        calculateColor(mBarPointerPosition);
        mBarPointerPaint.setColor(new Color(mColor));
        if (mPicker != null) {
            mPicker.setNewCenterColor(mColor);
            mPicker.changeOpacityBarColor(mColor);
        }
        invalidate();
    }

    /**
     * Calculate the color selected by the pointer on the bar.
     *
     * @param coord Coordinate of the pointer.
     */
    private void calculateColor(int coord) {
        coord = coord - mBarPointerHaloRadius;
        if (coord < 0) {
            coord = 0;
        } else if (coord > mBarLength) {
            coord = mBarLength;
        }
        mColor = HsvColor.toColor(mHSVColor.getAlpha(), mHSVColor.getHue(), (1 - (mPosToSatFactor * coord)) * 100, mHSVColor.getValue());
    }

    /**
     * Get the currently selected color.
     *
     * @return The ARGB value of the currently selected color.
     */
    public int getColor() {
        return mColor;
    }

    /**
     * Adds a {@code ColorPicker} instance to the bar. <br>
     * <br>
     * WARNING: Don't change the color picker. it is done already when the bar
     * is added to the ColorPicker
     *
     * @param picker
     * @see ColorPicker#addSVBar(SVBar)
     */
    public void setColorPicker(ColorPicker picker) {
        mPicker = picker;
    }

}
