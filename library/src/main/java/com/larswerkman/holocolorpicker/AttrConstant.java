/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.larswerkman.holocolorpicker;

public interface AttrConstant {

    String BAR_THICKNESS = "bar_thickness";
    String BAR_LENGTH = "bar_length";
    String BAR_POINTER_RADIUS = "bar_pointer_radius";
    String BAR_POINTER_HALO_RADIUS = "bar_pointer_halo_radius";
    String BAR_ORIENTATION_HORIZONTAL = "bar_orientation_horizontal";

    String COLOR_WHEEL_RADIUS = "color_wheel_radius";
    String COLOR_WHEEL_THICKNESS = "color_wheel_thickness";
    String COLOR_CENTER_RADIUS = "color_center_radius";
    String COLOR_CENTER_HALO_RADIUS = "color_center_halo_radius";
    String COLOR_POINTER_RADIUS = "color_pointer_radius";
    String COLOR_POINTER_HALO_RADIUS = "color_pointer_halo_radius";
}
